import React from "react";
import { useState, useEffect } from "react";
import queryString from "query-string";
export default function ReRouter(props) {
  const [data, setData] = useState({});

  useEffect(() => {
    const { model } = props.match.params;
    const parsed = queryString.parse(props.location.search);

    console.log(props.match.params, parsed);
    fetch(model + "?id=" + parsed.id + "&page=" + parsed.page)
      .then((response) => response.json())
      .then((data) => {
        const parsed = JSON.parse(data);
        //   parsed.nutrient_carb = parseFloat(parsed.nutrient_carb);
        //   parsed.nutrient_energy = parseFloat(parsed.nutrient_energy);
        //   parsed.nutrient_fat = parseFloat(parsed.nutrient_fat);
        //   parsed.nutrient_protein = parseFloat(parsed.nutrient_protein);

        setData(parsed);

        console.log(JSON.parse(data));
      });
  }, []);

  return <>{JSON.stringify(data)}</>;
}
