import React from "react";
import "./Pagination.css";
import { useState, useEffect } from "react";
import { Card, Form, Button } from "react-bootstrap";
import Highlighter from "react-highlight-words";
import {
  Table,
  TableRow,
  TableCell,
  TableHead,
  TableBody,
  TableSortLabel,
  Radio,
} from "@material-ui/core";
import "bootstrap/dist/css/bootstrap.min.css";

export default function Pagination({
  dataName,
  tHead,
  RenderComponent,
  title,
  pageLimit,
  dataLimit,
}) {
  const [pages, setPages] = useState(20);
  const [currentPage, setCurrentPage] = useState(1);
  const [data, setData] = useState([]);
  const [orderBy, setOrderBy] = React.useState("");
  const [order, setOrder] = React.useState(undefined);
  const [num_results, setNumResults] = React.useState(0);
  const [sortColNames, setSortColNames] = React.useState([]);
  const [filterColNames, setFilterColNames] = React.useState({});
  const [searchStr, setSearchStr] = React.useState("");
  const [highlightStrs, setHighlightStrs] = React.useState([]);
  useEffect(() => {
    console.log("page change is called in use effect", currentPage);
    var sortStr = "";

    for (var t of tHead) {
      t.ref = React.createRef();
    }

    for (var s of sortColNames) sortStr += s + ",";
    sortStr = "sortBy=" + sortStr.substring(0, sortStr.length - 1) + "&";
    if (sortColNames.length == 0) sortStr = "";

    var filterStr = "";
    for (const [key, value] of Object.entries(filterColNames)) {
      filterStr += key + "=" + value + "&";
      console.log("frefer filte", filterStr);
    }
    // filterStr = filterStr.substring(0, filterStr.length - 1);
    if (Object.keys(filterColNames).length == 0) filterStr = "";
    const url = `${dataName}?${sortStr}${filterStr}${searchStr}page=${
      currentPage - 1
    }`;
    console.log(
      "https://r1muet4wvk.execute-api.us-east-2.amazonaws.com/dev/" + url,
      "url"
    );
    //"https://r1muet4wvk.execute-api.us-east-2.amazonaws.com/dev/"
    // https://r1muet4wvk.execute-api.us-east-2.amazonaws.com/dev/recipes
    fetch("https://r1muet4wvk.execute-api.us-east-2.amazonaws.com/dev/" + url, {
      method: "GET",
      mode: "cors",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log("hi");
        const parsed = JSON.parse(data);
        console.log(parsed);
        setData(parsed.data);
        setPages(parsed.num_results / pageLimit);
        setNumResults(parsed.num_results);
        console.log("inside pagination", data);
      });
  }, [currentPage, sortColNames, filterColNames, searchStr]);

  function goToNextPage() {
    // not yet implemented
    setCurrentPage((page) => page + 1);
    return currentPage;
  }

  function goToPreviousPage() {
    // not yet implemented
    setCurrentPage((page) => page - 1);
  }

  function changePage(event) {
    // not yet implemented
    const pageNumber = Number(event.target.textContent);
    setCurrentPage(pageNumber);
  }

  const getPaginatedSortedData = () => {
    // not yet implemented
    // const startIndex = currentPage * dataLimit - dataLimit;
    // const endIndex = startIndex + dataLimit;
    // return stableSort(filterFn.fn(data), getComparator(order, orderBy)).slice(
    //   page * rowsPerPage,
    //   (page + 1) * rowsPerPage
    // );
    return data;
  };

  const getPaginationGroup = () => {
    // not yet implemented
    let start = Math.floor((currentPage - 1) / pageLimit) * pageLimit;
    return new Array(pageLimit).fill().map((_, idx) => start + idx + 1);
  };

  function handleSortRequest(columnId) {
    const isAsc = orderBy === columnId && order === "asc";
    setOrder(isAsc ? "desc" : "asc");
    setOrderBy(columnId);
  }

  function handleSortOnclick(e) {
    e.preventDefault();
    // console.log(e.target.elements); // from elements property
    // var i =0;
    var colNames = [];
    console.log(tHead);
    for (var h of tHead) {
      console.log(h, "h");
      if (h.disableSorting) continue;
      console.log(h.displayName, "debugsort");
      if (e.target[h.displayName].checked) {
        console.log("sortcols: ", h.queryName);
        colNames.push(h.queryName);
      }
      console.log(e.target[h.displayName].checked);
    }
    setSortColNames(colNames);
    // console.log(e.target.username.value);
  }

  function handleFilterOnSubmit(e) {
    e.preventDefault();
    console.log(e.target.elements, "help help help"); // from elements property
    // var i =0;
    var colNames = {};
    console.log(tHead);
    // var str = "";
    for (var h of tHead) {
      if (h.disableSorting) continue;
      if (h.filterOptions == "text") {
        if (e.target[h.displayName].value != "")
          colNames[h.queryName] = e.target[h.displayName].value;
      } else {
        console.log(h);
        if (
          h.ref == null ||
          h.ref.current == null ||
          h.ref.current.value == "default"
        )
          continue;
        if (h.queryName == "address" || h.tag == "nutritionName") {
          setSearchStr("search=" + h.ref.current.value + "&");
        } else {
          var val = h.ref.current.value;
          colNames[h.queryName] = "";
          if (h.tag != "text") colNames[h.queryName] = ",";
          colNames[h.queryName] += val;
        }
      }
      // var val = h.ref.current.value;
      // if (val != "default") {
      //   str += val + "+";
      // }
      // console.log(h.ref.current.value, "helping pls work");

      // console.log(h, "ww");
      // if (h.disableSorting) continue;
      // console.log(h.displayName, e.target[h.displayName].value, "debugfilter");
      // if (e.target[h.displayName].value) {
      //   console.log("sortcols: ", h.queryName);
      //   colNames[h.queryName] = e.target[h.displayName].value;
      // }
      // console.log(e.target[h.displayName].checked);
    }
    // if (str.length > 0) {
    //   str = "search=" + str.substring(0, str.length - 1) + "&";
    //   setSearchStr(str);
    // } else {
    //   setSearchStr("");
    // }
    setFilterColNames(colNames);
    // setFilterColNames(colNames);
    console.log(colNames, "endans");
  }

  function handleSearchOnSubmit(e) {
    e.preventDefault();
    console.log(e.target.elements); // from elements property
    // var i =0;
    var colNames = e.target["searchbar"].value;
    console.log("orig", colNames);
    colNames = colNames.split(" ");
    console.log(tHead);
    var str = "";
    for (var c of colNames) {
      str += c + "+";
    }
    console.log(colNames, "Colnames");
    if (colNames.length > 0 && colNames[0] != "") {
      str = "search=" + str.substring(0, str.length - 1) + "&";
      setSearchStr(str);
    } else {
      setSearchStr("");
    }
    console.log(searchStr, "searchstr");
    setHighlightStrs(colNames);
    // setFilterColNames(colNames);
    console.log(colNames, "endans");
  }

  console.log("is this working");
  return (
    <div>
      <h1>{title}</h1>
      <h6>{num_results} results found</h6>
      {/* show the posts, 10 posts at a time */}
      <Card>
        <Card.Title>
          Sort By (can only sort by one column at a time):
        </Card.Title>
        <Card.Body>
          <Form onSubmit={handleSortOnclick}>
            {tHead.map((colName, i) => {
              console.log(i);
              if (colName.disableSorting != true) {
                return (
                  <div key={`inline-radio-` + i} className="mb-3">
                    <Form.Check
                      inline
                      label={colName.displayName}
                      name={colName.displayName}
                      type={"switch"}
                      id={`inline-radio-` + i}
                    />
                  </div>
                );
              }
            })}
            <Button variant="primary" type="submit">
              Submit
            </Button>
          </Form>
        </Card.Body>
      </Card>
      <Card>
        <Card.Title>
          Filter By (for numerical values, filter by less than or equal to
          value):
        </Card.Title>
        <Card.Body>
          <Form onSubmit={handleFilterOnSubmit}>
            {tHead.map((colName, i) => {
              if (
                colName.disableSorting != true &&
                colName.filterOptions != "text"
              ) {
                return (
                  <div key={`inline-radio` + i} className="mb-3">
                    {/* <Form.Check
                      inline
                      label={colName.displayName}
                      // name="group1"
                      type={"radio"}
                      id={`inline-radio-1`}
                    /> */}
                    {/* 
                    {tHead.map((colName, i) => { */}

                    <Form.Select
                      key={i}
                      name={colName.displayName}
                      aria-label="Default select example"
                      ref={colName.ref}
                    >
                      <option value="default" key={"0" + colName}>
                        Filter by {colName.displayName}
                      </option>
                      {colName.filterOptions.map((filter, i) => {
                        return (
                          <option value={filter} key={colName + i}>
                            {filter}
                          </option>
                        );
                      })}
                    </Form.Select>

                    {/* })} */}
                    {/* <Form.Group className="mb-3" controlId="formBasicPassword">
                      <Form.Label>{colName.displayName}</Form.Label>
                      <Form.Control
                        type="text"
                        name={colName.displayName}
                        placeholder="filter"
                      />
                    </Form.Group> */}
                  </div>
                );
              } else if (colName.filterOptions == "text") {
                return (
                  <Form.Group className="mb-3" controlId="formBasicPassword">
                    <Form.Label>{colName.displayName}</Form.Label>
                    <Form.Control
                      type="text"
                      name={colName.displayName}
                      placeholder="filter"
                    />
                  </Form.Group>
                );
              }
            })}
            <Button variant="primary" type="submit">
              Submit
            </Button>
          </Form>
        </Card.Body>
      </Card>
      <Card>
        <Card.Title>Search Bar:</Card.Title>
        <Card.Body>
          <Form onSubmit={handleSearchOnSubmit}>
            <Form.Group className="mb-3" controlId="formBasicPassword">
              <Form.Label>{"Search Bar"}</Form.Label>
              <Form.Control
                type="text"
                name={"searchbar"}
                placeholder="Search"
              />
            </Form.Group>
            <Button variant="primary" type="submit">
              Submit
            </Button>
          </Form>
        </Card.Body>
      </Card>
      <div className="dataContainer">
        <Table>
          <TableHead>
            <TableRow>
              {tHead.map((obj) => {
                return (
                  <TableCell sortDirection={orderBy === obj.id ? order : false}>
                    {obj.disableSorting ? (
                      obj.displayName
                    ) : (
                      <TableSortLabel
                        active={orderBy === obj.id}
                        direction={orderBy === obj.id ? order : "asc"}
                        onClick={() => {
                          handleSortRequest(obj.id);
                        }}
                      >
                        {obj.displayName}
                      </TableSortLabel>
                    )}
                  </TableCell>
                );
              })}
            </TableRow>
          </TableHead>

          <TableBody>
            {getPaginatedSortedData().map((d, idx) => {
              console.log(d, "testing");
              return (
                <RenderComponent
                  key={idx}
                  data={d}
                  highlightStrs={highlightStrs}
                />
              );
            })}
          </TableBody>
        </Table>
      </div>
      {/* show the pagiantion
      it consists of next and previous buttons
      along with page numbers, in our case, 5 page
      numbers at a time
  */}
      <div className="pagination">
        {/* previous button */}
        <button
          onClick={goToPreviousPage}
          className={`prev ${currentPage === 1 ? "disabled" : ""}`}
        >
          prev
        </button>

        {/* show page numbers */}
        {getPaginationGroup().map((item, index) => (
          <button
            key={index}
            onClick={changePage}
            className={`paginationItem ${
              currentPage === item ? "active" : null
            }`}
          >
            <span>{item}</span>
          </button>
        ))}

        {/* next button */}
        <button
          onClick={goToNextPage}
          className={`next ${currentPage === pages ? "disabled" : ""}`}
        >
          next
        </button>
      </div>
    </div>
  );
}
