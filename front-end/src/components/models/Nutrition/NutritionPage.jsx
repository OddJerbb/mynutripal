import React from "react";
import ReactPlayer from "react-player";
import { useEffect, useState } from "react";
import { Chart } from "react-google-charts";

export default function NutritionPage(props) {
  const [data, setData] = useState({});

  const getProfile = (props) => {
    // <-- function takes props object!!
    console.log(props);
    const { id } = props.match.params;

    fetch(
      "https://r1muet4wvk.execute-api.us-east-2.amazonaws.com/dev/nutritionalInfo?id=" +
        id
    )
      .then((response) => response.json())
      .then((data) => {
        const parsed = JSON.parse(data)["data"][0];
        parsed.nutrient_carb = parseFloat(parsed.nutrient_carb);
        parsed.nutrient_energy = parseFloat(parsed.nutrient_energy);
        parsed.nutrient_fat = parseFloat(parsed.nutrient_fat);
        parsed.nutrient_protein = parseFloat(parsed.nutrient_protein);

        setData(parsed);

        console.log(JSON.parse(data));
      });
  };

  useEffect(() => {
    getProfile(props);
  }, []);

  return (
    <div style={{ backgroundColor: "white", marginTop: "10px" }}>
      <div>
        Below image shows which nutrient source is rich in this item. Muscle
        shows that this item is dominant in protein. Scoop of powder shows
        dominance in fat. Energy shows dominance in carbs
      </div>
      {data.nutrient_carb > data.nutrient_fat &&
        data.nutrient_carb > data.nutrient_protein && (
          <img
            src="https://cdn-icons-png.flaticon.com/512/603/603884.png"
            width="200px"
          />
        )}
      {data.nutrient_protein > data.nutrient_carb &&
        data.nutrient_protein > data.nutrient_fat && (
          <img
            src="https://cdn.iconscout.com/icon/free/png-256/muscle-1575184-1331518.png"
            width="200px"
          />
        )}
      {data.nutrient_fat > data.nutrient_carb &&
        data.nutrient_fat > data.nutrient_protein && (
          <img
            src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQgSoV2dV1EbDO8su_6c-beNmRczKJHYkkF7w&usqp=CAU"
            width="200px"
          />
        )}
      <h3>Item name: {data.name}</h3>
      <h4>Nutritional info about this item is shown below</h4>
      <p style={{ fontSize: "20px" }}>Carbs: {data.nutrient_carb}</p>
      <p>Energy: {data.nutrient_energy}</p>
      <p>Protein: {data.nutrient_protein}</p>
      <p>Fat: {data.nutrient_fat}</p>
      <p style={{ fontWeight: "bold" }}>Relevant Recipes:</p>
      <a
        href={
          "/Recipe/" +
          (props.match.params.id > 200 ? 199 : props.match.params.id)
        }
      >
        Recipe
      </a>
      <br />
      <br />
      <p style={{ fontWeight: "bold" }}>Served At:</p>
      <a
        href={
          "/Restaurant/" +
          (props.match.params.id > 200 ? 199 : props.match.params.id)
        }
      >
        Restaurant
      </a>
      <br />
      <br />
      <Chart
        width={"500px"}
        height={"300px"}
        chartType="PieChart"
        loader={<div>Loading Chart</div>}
        data={[
          ["Nutrient", "Grams"],
          ["Fat", parseFloat(data.nutrient_fat)],
          ["Carbs", parseFloat(data.nutrient_carb)],
          ["Protein", parseFloat(data.nutrient_protein)],
        ]}
        options={{
          title: "Nutritional info",
        }}
        rootProps={{ "data-testid": "1" }}
      />
    </div>
  );
}
