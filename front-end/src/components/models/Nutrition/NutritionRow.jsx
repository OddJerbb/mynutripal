import React from "react";
import { Button } from "react-bootstrap";
import { useHistory } from "react-router-dom";
import { TableRow, TableCell } from "@material-ui/core";
import Highlighter from "react-highlight-words";
export default function NutritionRow(props) {
  const history = useHistory();
  const handleOnClick = (url) => {
    history.push("Nutrition/" + url);
  };

  function highlight(text) {
    return (
      <Highlighter
        highlightClassName="YourHighlightClass"
        searchWords={props.highlightStrs}
        autoEscape={true}
        textToHighlight={String(text)}
      />
    );
  }

  return (
    <TableRow>
      <TableCell>{highlight(props.data.id)}</TableCell>
      <TableCell>{highlight(props.data.name)}</TableCell>
      <TableCell>{highlight(props.data.nutrient_protein)}</TableCell>
      <TableCell>{highlight(props.data.nutrient_energy)}</TableCell>
      <TableCell>{highlight(props.data.nutrient_fat)}</TableCell>
      <TableCell>{highlight(props.data.nutrient_carb)}</TableCell>

      <TableCell>
        <Button
          onClick={(e) => {
            handleOnClick(props.data.id);
          }}
        >
          Learn More
        </Button>
      </TableCell>
    </TableRow>
  );
}
