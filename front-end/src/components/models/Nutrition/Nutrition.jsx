import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { useState } from "react";
import NutritionRow from "./NutritionRow";
import Pagination from "../../utils/Pagination";

export default function Nutrition() {
  const [tHead, setTHead] = useState([
    { displayName: "id", disableSorting: true },
    {
      displayName: "Name",
      tag: "nutritionName",
      queryName: "name",
      filterOptions: ["raw", "beverage"],
    },
    {
      displayName: "Protein",
      queryName: "nutrient_protein",
      filterOptions: ["0.1", "0.2", "0.3"],
    },
    {
      displayName: "Calories",
      queryName: "nutrient_energy",
      filterOptions: ["100", "200", "300", "400", "500"],
    },
    {
      displayName: "Fat",
      queryName: "nutrient_fat",
      filterOptions: ["0", "0.1", "0.2"],
    },
    {
      displayName: "Sugar",
      queryName: "nutrient_carb",
      filterOptions: ["0", "0.1", "0.2"],
    },
  ]);

  return (
    <div style={{ backgroundColor: "white", marginTop: "10px" }}>
      {/* <p style={{ marginLeft: "10px" }}>10 results found:</p> */}
      <Pagination
        dataName={"nutritionalInfo"}
        tHead={tHead}
        RenderComponent={NutritionRow}
        title="Nutrition"
        pageLimit={20}
        dataLimit={10}
      />
    </div>
  );
}
