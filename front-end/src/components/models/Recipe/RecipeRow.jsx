import React from "react";
import { Button } from "react-bootstrap";
import { useHistory } from "react-router-dom";
import Highlighter from "react-highlight-words";
import { TableRow, TableCell } from "@material-ui/core";
export default function RecipeRow(props) {
  const history = useHistory();
  const handleOnClick = (url) => {
    history.push("Recipe/" + url);
  };
  const processYoutubeURL = (url) => {
    return "https://www.youtube.com/embed/" + url.split("=").pop();
  };

  function highlight(text) {
    return (
      <Highlighter
        highlightClassName="YourHighlightClass"
        searchWords={props.highlightStrs}
        autoEscape={true}
        textToHighlight={String(text)}
      />
    );
  }
  return (
    <TableRow>
      <TableCell>{highlight(props.data.id)}</TableCell>

      <TableCell>{highlight(props.data.name)}</TableCell>
      <TableCell>{highlight(props.data.category)}</TableCell>
      <TableCell>{highlight(props.data.origin)}</TableCell>
      <TableCell>{highlight(props.data.difficulty)}</TableCell>
      <TableCell>
        {highlight(props.data.tags == null ? "n/a" : props.data.tags)}
      </TableCell>
      <TableCell>
        <Button
          onClick={(e) => {
            handleOnClick(props.data.id);
          }}
        >
          Learn More
        </Button>
      </TableCell>
    </TableRow>
  );
}
