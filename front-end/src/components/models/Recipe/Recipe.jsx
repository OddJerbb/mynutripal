import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { useState } from "react";
import { useHistory } from "react-router-dom";
import Pagination from "../../utils/Pagination";
import RecipeRow from "./RecipeRow";
export default function Recipe() {
  const [tHead, setTHead] = useState([
    { displayName: "Id", disableSorting: true },
    {
      displayName: "Recipe Name",
      queryName: "name",
      tag: "nutritionName",
      filterOptions: ["Chicken", "Tart"],
    },
    {
      displayName: "Category",
      queryName: "category",
      tag: "text",
      filterOptions: ["Dessert", "Vegetarian"],
    },
    {
      displayName: "Origin",
      queryName: "origin",
      tag: "text",
      filterOptions: ["Italian", "American", "Mexican"],
    },
    {
      displayName: "Difficulty",
      queryName: "difficulty",
      tag: "text",
      filterOptions: ["1", "2", "3", "4", "5"],
    },
    {
      displayName: "Tags",
      queryName: "tags",
      tag: "nutritionName",
      filterOptions: ["Pasta", "Dairy"],
    },
  ]);

  return (
    <div style={{ backgroundColor: "white", marginTop: "10px" }}>
      {/* <p style={{ marginLeft: "10px" }}>10 results found:</p> */}
      <Pagination
        dataName={"recipes"}
        tHead={tHead}
        RenderComponent={RecipeRow}
        title="Recipes"
        pageLimit={20}
        dataLimit={10}
      />
    </div>
  );
}
