import React from "react";
import ReactPlayer from "react-player";
import { useEffect, useState } from "react";
export default function RecipePage(props) {
  const [data, setData] = useState({});

  const getProfile = (props) => {
    // <-- function takes props object!!
    console.log(props);
    const { id } = props.match.params;

    fetch(
      "https://r1muet4wvk.execute-api.us-east-2.amazonaws.com/dev/recipes?id=" +
        id
    )
      .then((response) => response.json())
      .then((data) => {
        const parsed = JSON.parse(data);

        setData(parsed["data"][0]);

        console.log(JSON.parse(data));
      });
  };

  useEffect(() => {
    getProfile(props);
  }, []);

  return (
    <div id="container" style={{ backgroundColor: "white", marginTop: "10px" }}>
      <h3>{data.name}</h3>

      <br />

      <p style={{ fontWeight: "bold" }}>Recipe:</p>
      <p style={{ fontSize: "20px" }}>{data.instructions}</p>

      <br />

      <p style={{ fontWeight: "bold" }}>Relevant Nutritional Stats:</p>
      <a
        href={
          "/Nutrition/" +
          [366, 367, 368, 369, 370, 371, 372, 373, 374, 375][
            props.match.params.id % 10
          ]
        }
      >
        Nutrition
      </a>

      <p style={{ fontWeight: "bold" }}>Relevant Restaurants:</p>
      <a href={"/Restaurant/" + (props.match.params.id + 1)}>Restaurant</a>

      <br />

      <p style={{ fontWeight: "bold" }}>Video Recipe:</p>
      <div>
        <ReactPlayer url={data.youtube_url} />
      </div>
      <br />
      <img src={data.thumbnail} />
    </div>
  );
}
