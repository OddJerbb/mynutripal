import React from "react";
import { Button } from "react-bootstrap";
import { useHistory } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import Highlighter from "react-highlight-words";
import { TableRow, TableCell } from "@material-ui/core";
export default function RestaurantRow(props) {
  const history = useHistory();

  const handleOnClick = (url) => {
    history.push("Restaurant/" + url);
  };

  function highlight(text) {
    return (
      <Highlighter
        highlightClassName="YourHighlightClass"
        searchWords={props.highlightStrs}
        autoEscape={true}
        textToHighlight={String(text)}
      />
    );
  }
  //
  return (
    <TableRow>
      <TableCell>{highlight(props.data.restaurant_name)}</TableCell>
      <TableCell>{highlight(props.data.avg_price)}</TableCell>
      <TableCell>{highlight(props.data.distance)}</TableCell>
      <TableCell>{highlight(props.data.address)}</TableCell>
      <TableCell>{highlight(props.data.rating)}</TableCell>
      <TableCell>
        <Button
          variant="outline-secondary"
          onClick={(e) => {
            handleOnClick(props.data.id);
          }}
        >
          Learn More
        </Button>
      </TableCell>
    </TableRow>
  );
}
