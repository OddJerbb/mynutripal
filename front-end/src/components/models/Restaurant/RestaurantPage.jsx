import React from "react";
import ReactPlayer from "react-player";
import { useEffect, useState } from "react";
import env from "react-dotenv";
export default function RestaurantPage(props) {
  const [data, setData] = useState({});

  const getProfile = (props) => {
    // <-- function takes props object!!
    console.log(props);
    const { id } = props.match.params;

    fetch(
      "https://r1muet4wvk.execute-api.us-east-2.amazonaws.com/dev/restaurants?id=" +
        id
    )
      .then((response) => response.json())
      .then((data) => {
        const parsed = JSON.parse(data);

        setData(parsed["data"][0]);

        console.log(JSON.parse(data));
      });
  };

  useEffect(() => {
    getProfile(props);
  }, []);

  return (
    <div style={{ backgroundColor: "white", marginTop: "10px" }}>
      <h3>{data.restaurant_name}</h3>
      <p style={{ fontSize: "20px" }}>
        This restaurant is located at {data.address}
      </p>
      <p>Restaurant's phone number is {data.restaurant_phone}</p>

      {/* <iframe
        src={data.restaurant_website}
        height="800"
        width="800"
        title="Iframe Example"
      ></iframe> */}
      <p>
        <a href={data.restaurant_website}>website</a>
      </p>

      <iframe
        width="600"
        height="450"
        style={{ border: "0" }}
        loading="lazy"
        allowfullscreen
        src={
          "https://www.google.com/maps/embed/v1/place?key=" +
          "AIzaSyBAPUHPkHgMxJptQtN-b-WjohT8OkxQUB4" +
          "&q=" +
          data.address
        }
      ></iframe>

      <p style={{ fontWeight: "bold" }}>Relevant Recipes:</p>
      <a href={"/Recipe/" + props.match.params.id}>Recipe</a>
      <br />

      <p style={{ fontWeight: "bold" }}>Relevant Nutritional Stats:</p>
      <a
        href={
          "/Nutrition/" +
          [366, 367, 368, 369, 370, 371, 372, 373, 374, 375][
            props.match.params.id % 10
          ]
        }
      >
        Nutrition
      </a>
      <br />
    </div>
  );
}
