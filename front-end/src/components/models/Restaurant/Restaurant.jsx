import "bootstrap/dist/css/bootstrap.min.css";
import { useState } from "react";
import RestaurantRow from "./RestaurantRow";
import Pagination from "../../utils/Pagination";
export default function Restaurant() {
  const [tHead, setTHead] = useState([
    {
      displayName: "Restaurant Name",
      queryName: "restaurant_name",
      filterOptions: "text",
    },
    {
      displayName: "Avg Price (less than equal)",
      queryName: "avg_price",
      filterOptions: ["10", "9", "11"],
    },
    {
      displayName: "Distance from UT Tower (miles)",
      queryName: "distance",
      filterOptions: ["0.31", "0.43", "0.51"],
    },
    {
      displayName: "Address",
      queryName: "address",
      filterOptions: ["Rio Grande", "Guadalupe"],
    },
    {
      displayName: "Rating",
      queryName: "rating",
      // disableSorting: true,
      filterOptions: ["1", "2", "3", "4", "5"],
    },
  ]);

  return (
    <div id="container" style={{ backgroundColor: "white", marginTop: "10px" }}>
      {/* <p style={{ marginLeft: "10px" }}>10 results found:</p> */}
      <Pagination
        dataName={"restaurants"}
        tHead={tHead}
        RenderComponent={RestaurantRow}
        title="Restaurants"
        pageLimit={20}
        dataLimit={10}
      />
    </div>
  );
}
