import React from "react";
import "./Navbar.css";
import { Nav, NavDropdown, Container, Navbar } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
class MyNavbar extends React.Component {
  render() {
    return (
      <div>
        <Navbar bg="light" expand="lg">
          <Container>
            <Navbar.Brand href="/home">MyNutripal</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
              <Nav className="me-auto">
                <Nav.Link href="/home">Home</Nav.Link>
                <Nav.Link href="/about">About</Nav.Link>
                <NavDropdown title="Models" id="basic-nav-dropdown">
                  <NavDropdown.Item href="/restaurant">
                    Restaurants
                  </NavDropdown.Item>
                  <NavDropdown.Item href="/nutrition">
                    Nutrition
                  </NavDropdown.Item>
                  <NavDropdown.Item href="/recipe">Recipes</NavDropdown.Item>
                  <NavDropdown.Divider />
                </NavDropdown>
                <Nav.Link href="/visuals">Visualizations</Nav.Link>
              </Nav>
            </Navbar.Collapse>
          </Container>
        </Navbar>

        {/* <nav>
          <ul style={{ marginBottom: "0" }} id="navbar">
            <li>
              <a href="/Home">Home</a>
            </li>
            <li>
              <a href="/Nutrition">Nutrition</a>
            </li>
            <li>
              <a href="/Recipe">Recipes</a>
            </li>
            <li>
              <a href="/Restaurant">Restaurants</a>
            </li>
            <li>
              <a href="/About">About</a>
            </li>
          </ul>
        </nav> */}
      </div>
    );
  }
}

export default MyNavbar;
