// import React from "react";
import rd3 from "react-d3-library";
// import { useEffect, useState } from "react";
import node from "./barChart";
// import node from "./scatterChart";
import React, { useEffect, useState } from "react";
import {
  Bar,
  BarChart,
  CartesianGrid,
  Legend,
  Scatter,
  ScatterChart,
  Tooltip,
  XAxis,
  YAxis,
  LineChart,
  Line,
  ZAxis,
  PieChart,
  Pie,
  Cell,
  Label,
} from "recharts";
const RD3Component = rd3.Component;
const BarChartRD = rd3.BarChart;
// const ScatterChartRD = rd3.ScatterChart;
export default function Visualization() {
  const [nutritionData, setNutritionData] = useState([]);
  const [restaurantData, setRestaurantData] = useState([]);

  const [data, setData] = useState({
    d3: "",
    width: 500,
    height: 750,
    dataset: [
      { label: "apples", value: 25 },
      { label: "oranges", value: 30 },
      { label: "surfboards", value: 150 },
    ],
  });
  const [data2, setData2] = useState({
    d3: "",
    width: 500,
    height: 750,
    dataset: [
      { label: "apples", value: 25 },
      { label: "oranges", value: 30 },
      { label: "surfboards", value: 150 },
    ],
  });

  useEffect(() => {
    setData(node);
    // setData2(node);
    fetch(
      "https://r1muet4wvk.execute-api.us-east-2.amazonaws.com/dev/nutritionalInfo",
      {
        method: "GET",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
      }
    )
      .then((response) => response.json())
      .then((data) => {
        const res = JSON.parse(data).data.map((d) => {
          return {
            name: d.name,
            protein: d.nutrient_protein,
            fat: d.nutrient_fat,
            carb: d.nutrient_carb,
          };
        });
        console.log(res);
        setNutritionData(res);
      });

    fetch(
      "https://r1muet4wvk.execute-api.us-east-2.amazonaws.com/dev/restaurants",
      {
        method: "GET",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
      }
    )
      .then((response) => response.json())
      .then((data) => {
        const res = JSON.parse(data).data.map((d) => {
          return {
            name: d.restaurant_name,
            z: d.rating,
            x: d.avg_price,
            y: d.distance,
          };
        });
        console.log(res);
        setRestaurantData(res);
      });

    // fetch("https://api.driveresponsibly.me/api/brewery", {
    //   method: "GET",
    //   headers: {
    //     Accept: "application/vnd.api+json",
    //     "Content-Type": "application/vnd.api+json",
    //   },
    // })
    //   .then((response) => response.json())
    //   .then((data) => {
    //     const res = JSON.parse(data);
    //     console.log(res);
    //   });
  }, []);

  const data01 = [
    { x: 100, y: 200, z: 200 },
    { x: 120, y: 100, z: 260 },
    { x: 170, y: 300, z: 400 },
    { x: 140, y: 250, z: 280 },
    { x: 150, y: 400, z: 500 },
    { x: 110, y: 280, z: 200 },
  ];

  const datax = [
    { name: "Bubble Tea Sold", value: 10 },
    { name: "Bubble Tea Left", value: 4 },
  ];

  return (
    <div style={{ backgroundColor: "white" }}>
      <div>
        <b>MyNutriPal Visualizations</b>
      </div>
      <BarChart
        width={500}
        height={300}
        data={nutritionData}
        margin={{
          top: 5,
          right: 30,
          left: 20,
          bottom: 5,
        }}
      >
        <CartesianGrid strokeDasharray="3 3" />
        <XAxis dataKey="name" />
        <YAxis />
        <Tooltip />
        <Legend />
        <Bar dataKey="protein" fill="#8884d8" />
        <Bar dataKey="carb" fill="#82ca9d" />
        <Bar dataKey="fat" fill="#ff00ff" />
      </BarChart>

      <ScatterChart
        width={400}
        height={400}
        margin={{
          top: 20,
          right: 20,
          bottom: 20,
          left: 20,
        }}
      >
        <CartesianGrid />
        <XAxis type="number" dataKey="x" name="Average Price" unit="$" />
        <YAxis type="number" dataKey="y" name="Distance" unit="mi" />
        <ZAxis
          type="number"
          dataKey="z"
          // range={[0, 5]}
          name="Rating"
          unit="*"
        />
        <Tooltip cursor={{ strokeDasharray: "3 3" }} />
        <Legend />
        <Scatter
          name="Restaurant"
          data={restaurantData}
          fill="#8884d8"
          shape="star"
        />
      </ScatterChart>

      <LineChart
        width={800}
        height={250}
        data={[
          {
            name: "Crock Pot Baked Tacos",
            Difficulty: 2,
            // "Procuct B": 2342,
          },
          {
            name: "Alfredo Primavera",
            Difficulty: 2,
            // "Procuct B": 3246,
          },
          //   {
          //     name: "Nutty Chicken Curry",
          //     Difficulty: 1,
          //     // "Procuct B": 4556,
          //   },
          {
            name: "Enchilada Casserole",
            Difficulty: 2,
            // "Procuct B": 4465,
          },
          {
            name: "Marengo",
            Difficulty: 3,
            // "Procuct B": 4553,
          },
        ]}
        margin={{ top: 5, right: 30, left: 20, bottom: 5 }}
      >
        <CartesianGrid strokeDasharray="3 3" />
        <XAxis dataKey="name" />
        <YAxis />
        <Tooltip />
        <Legend />
        <Line type="monotone" dataKey="Difficulty" stroke="#0095FF" />
        {/* <Line type="monotone" dataKey="Procuct B" stroke="#FF0000" /> */}
      </LineChart>

      <div>
        <b>Drive Responsibly Visualizations</b>
      </div>
      <ScatterChart
        width={400}
        height={400}
        margin={{
          top: 20,
          right: 20,
          bottom: 20,
          left: 20,
        }}
      >
        <CartesianGrid />
        <XAxis type="number" dataKey="x" name="Population" />
        <YAxis type="number" dataKey="y" name="Poverty" />
        <ZAxis
          type="number"
          dataKey="z"
          // range={[0, 5]}
          name="County name"
          unit="*"
        />
        <Tooltip cursor={{ strokeDasharray: "3 3" }} />
        <Legend />
        <Scatter
          name="County"
          data={[
            {
              x: 57922,
              y: 8702,
              z: "Anderson",
            },
            {
              x: 18610,
              y: 1888,
              z: "Andrews",
            },
            {
              x: 86395,
              y: 13131,
              z: "Angelina",
            },
            {
              x: 23830,
              y: 4313,
              z: "Aransas",
            },
            {
              x: 8560,
              y: 877,
              z: "Archer",
            },
            {
              x: 1848,
              y: 194,
              z: "Armstrong",
            },
          ]}
          fill="#8884d8"
          shape="star"
        />
      </ScatterChart>
      <LineChart
        width={730}
        height={250}
        data={[
          {
            name: "Bee",
            "Unemployment Rate": 9.8,
            // "Procuct B": 2342,
          },
          {
            name: "Bell",
            "Unemployment Rate": 7,
            // "Procuct B": 3246,
          },
          {
            name: "Bexar",
            "Unemployment Rate": 7.5,
            // "Procuct B": 4556,
          },
          {
            name: "Blanco",
            "Unemployment Rate": 3.8,
            // "Procuct B": 4465,
          },
          {
            name: "Borden",
            "Unemployment Rate": 3.3,
            // "Procuct B": 4553,
          },
        ]}
        margin={{ top: 5, right: 30, left: 20, bottom: 5 }}
      >
        <CartesianGrid strokeDasharray="3 3" />
        <XAxis dataKey="name" />
        <YAxis />
        <Tooltip />
        <Legend />
        <Line type="monotone" dataKey="Unemployment Rate" stroke="#0095FF" />
        {/* <Line type="monotone" dataKey="Procuct B" stroke="#FF0000" /> */}
      </LineChart>

      <BarChartRD data={data} />
      {/* <ScatterChartRD data={data2} /> */}
      {/* <div>test</div> */}
    </div>
  );
}
