// Build data for a classic bar chart
const data = {};

// Labels are displayed in component, quantities are calculated to define height of each bar
data.dataSet = [
  { label: "Abilene", value: 3 },
  { label: "Addison", value: 2 },
  { label: "Allen", value: 2 },
  { label: "Alvin", value: 1 },
  { label: "Amarillo", value: 1 },
  { label: "Arlington", value: 3 },
  { label: "Athens", value: 1 },
  { label: "Austin", value: 23 },
];

//Set margins for bar graph within svg element
data.margins = { top: 20, right: 20, bottom: 70, left: 40 };

//Define label of y-axis
data.yAxisLabel = "Frequency";

// Colors are optional for each bar
// If colors are not given, bars will default to 'steelblue'
data.fill = [];

//Define the width of the svg element on the page
data.width = 960;

//Define the height of the bar chart
data.height = 700;

// Define tick intervals for y-axis
data.ticks = 15;

//Define a class for the svg element for styling
data.barClass = "bar";

/* EXAMPLE CSS
.bar text {
  font: 14px sans-serif;
  text-anchor: middle;
}
*/

export default data;
