import React, { useEffect, useState } from "react";
import "./AboutPage.css";
import aboutlogo from "./AboutLogo.png";
import nhPhoto from "./nhPhoto.png";
import jkPhoto from "./jkPhoto.png";
import hjPhoto from "./hjPhoto.png";
import blPhoto from "./blPhoto.png";
import tqPhoto from "./tqPhoto.png";

const userInfo = [
  // {
  //   name: "Nicholas Huang",
  //   username: "OddJerbb",
  //   commits: 0,
  //   issues: 0,
  //   tests: 0,
  // },
  {},
  {
    name: "Benjamin Li",
    username: "BenjaminGuLi",
    commits: 0,
    issues: 0,
    tests: 10,
  },
  {
    name: "Timothy Qin",
    username: "timothytqin",
    commits: 0,
    issues: 0,
    tests: 0,
  },
  {
    name: "Jibran Khalil",
    username: "Jibran12345",
    commits: 0,
    issues: 0,
    tests: 0,
  },
  {
    name: "Harshavardhan",
    username: "harsha31.jagan",
    commits: 0,
    issues: 0,
    tests: 20,
  },
];

const getGitLabInfo = async () => {
  var totalCommits = 0,
    totalIssues = 0,
    totalTests = 0;

  var commitList = await fetch(
    "https://gitlab.com/api/v4/projects/30021986/repository/contributors?order_by=email"
  );
  commitList = await commitList.json();

  commitList.forEach((commit) => {
    const { name, commits } = commit;
    userInfo.forEach((user) => {
      console.log(name);
      if (user.name === name || user.username === name) {
        user.commits += commits;
        totalCommits += commits;
      }
    });
  });
  console.log(userInfo, commitList);
  var pagenum = 1;
  var currentPage = [];
  let issueArr = [];
  do {
    currentPage = await fetch(
      `https://gitlab.com/api/v4/projects/30021986/issues?per_page=100&page=${pagenum}`
    );
    currentPage = await currentPage.json();
    issueArr = [...issueArr, ...currentPage];
    pagenum += 1;
  } while (currentPage.length === 100);

  issueArr.forEach((issue) => {
    const { assignees } = issue;
    assignees.forEach((assignee) => {
      const { name } = assignee;
      userInfo.forEach((user) => {
        if (user.name === name || user.username === name) {
          user.issues += 1;
        }
      });
    });
    totalIssues += 1;
  });

  return {
    totalCommits: totalCommits,
    totalIssues: totalIssues,
    totalTests: totalTests,
  };
};

function DevInfo(props) {
  return (
    <div class="developer">
      <div
        style={{
          width: "500px",
          marginRight: "100px",
          marginLeft: "100px",
          textAlign: "justify",
        }}
      >
        <span class="name">{props.name}</span>
        <br />
        <br />
        <span style={{ fontSize: "20px" }}>{props.team}</span>
        <br />
        <br />
        <span>{props.bio}</span>
      </div>
      <div style={{ width: "600px" }}>
        <p>Commits : {props.commits}</p>
        <p>Assigned Issues : {props.issues}</p>
        <p>Unit Tests : {props.tests}</p>
      </div>
      <img src={props.img} style={{ height: "275px" }} />
    </div>
  );
}

function AboutPage() {
  const [loaded, setLoaded] = useState(false);
  const [totalCommits, setTotalCommits] = useState(0);
  const [totalIssues, setTotalIssues] = useState(0);
  const [totalTests, setTotalTests] = useState(0);
  const [data, setData] = useState([]);

  useEffect(() => {
    document.title = "My Nutripal";
    const getGitLabData = async () => {
      const data = await getGitLabInfo();
      console.log(data);
      setTotalCommits(data.totalCommits);
      setTotalIssues(data.totalIssues);
      setTotalTests(30); // CHANGE IN THE FUTURE
      setLoaded(true);
    };
    getGitLabData();
  }, []);

  return (
    <div>
      <div id="header">
        <span style={{ fontSize: "100px" }}>About Us</span>
      </div>
      <div id="about">
        <center>
          <br />
          <p style={{ fontSize: "40px", fontFamily: "Garamond Bold" }}>
            Unsure about what to eat? We're here to help!
          </p>
          <div id="ourGoal">
            <div style={{ width: "600px", marginRight: "10%" }}>
              <span>
                We understand the importance of keeping healthy in terms of what
                we eat. Our goal is to give you all the information you need to
                plan your meals and maintain your balanced diet! From data
                ranging from nutritional information, restaurants, and recipes
                for you, we will be here to help every step of the way! Welcome
                to MyNutripal!
              </span>
            </div>
            <img style={{ height: "275px" }} src={aboutlogo} />
          </div>
          <div style={{ width: "935px", marginBottom: "35px" }}>
            <span style={{ fontSize: "20px" }}>
              FUN FACT: When putting all the data together, we found that
              restaurants that specified in fast food cuisine work mostly with
              ingredients that are high in calories and fats.
            </span>
          </div>
        </center>
      </div>

      <div id="developers">
        <DevInfo
          name="Benjamin Li"
          team="Back-end Team"
          bio="Benjamin Li is currently a junior at the University of Texas at Austin studying computer science. In his free time, he enjoys playing table tennis and board/strategy games."
          img={blPhoto}
          commits={loaded ? userInfo[1].commits : "?"}
          issues={loaded ? userInfo[1].issues : "?"}
        />
        <hr />
        <DevInfo
          name="Harshavardha Jagannathan"
          team="Front-end Team"
          bio="Harsha is passionate about create software that can change people's lives for the better. He is a rising tech entrepreneur who is currently the Senior Executive Chairman, Head of Staff Engineers, and Principal outreach and marketing director at MyNutriPal."
          img={hjPhoto}
          commits={loaded ? userInfo[4].commits : "?"}
          issues={loaded ? userInfo[4].issues : "?"}
          tests={userInfo[4].tests}
        />
        <hr />
        <DevInfo
          name="Jibran Khalil"
          team="Front-end Team"
          bio="Jibran loves creating products that impact people and that have a tangible impact on their lives. He is currently a junior at UT Austin studying computer science!"
          img={jkPhoto}
          commits={loaded ? userInfo[3].commits : "?"}
          issues={loaded ? userInfo[3].issues : "?"}
        />
        <hr />
        {/* <DevInfo
          name="Nicholas Huang"
          team="Front-end Team"
          bio="Nicholas fell in love with programming since high school and has been coding ever since. Now, he is very proud to work as a developer for MyNutripal, working long hours to ensure that users get all the food planning information that they need!"
          img={nhPhoto}
          commits={loaded ? userInfo[0].commits : "?"}
          issues={loaded ? userInfo[0].issues : "?"}
        />
        <hr /> */}
        <DevInfo
          name="Timothy Qin"
          team="Back-end Team"
          bio="Timothy is a junior at UT Austin studying computer science who is graduating at the end of 2021. He is an active member of the hackathon community and loves to do them in his free time."
          img={tqPhoto}
          commits={loaded ? userInfo[2].commits : "?"}
          issues={loaded ? userInfo[2].issues : "?"}
        />
      </div>

      <div id="extraStats">
        <span>Total Commits: {loaded ? totalCommits : "?"}</span>
        <br />
        <span>Total Issues: {loaded ? totalIssues : "?"}</span>
        <br />
        <span>Total Tests: {loaded ? totalTests : "?"}</span>
        <br />
      </div>

      <div id="links" className="bgWhite">
        <p>
          Our MyNutriPal API can be viewed here:
          <br />
          <a href="https://documenter.getpostman.com/view/7697465/UV5f7ZG2">
            API Documentation
          </a>
        </p>
        <p>
          We plan on pinging the end points for our APIs to scrape the data from
          them. Our data sources used can be viewed here:
          <br />
          <a href="https://documenu.com/">Menu items api</a>
          <br />
          <a href="https://spoonacular.com/food-api">Recipes api</a>
          <br />
          <a href="https://fdc.nal.usda.gov/">Nutrition api</a>
        </p>
      </div>

      <div className="bgWhite">
        We used <a href="https://aws.amazon.com/">AWS</a> to host our site. We
        used <a href="https://www.postman.com/">postman</a> to write
        documentation for our api. We used{" "}
        <a href="https://reactjs.org/">react</a> to build the front end. We used
        <a href="https://domains.google/">google domain</a> to purchase our
        domain name. We used css, html to develop our site. We used{" "}
        <a href="https://about.gitlab.com/">gitlab api</a>
        to fet the commits and issues data for each developer.
      </div>
    </div>
  );
}

export default AboutPage;
