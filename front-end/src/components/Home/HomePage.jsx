import React from "react";
import "./HomePage.css";
import logo from "./HomeLogo.png";
import "bootstrap/dist/css/bootstrap.min.css";

class HomePage extends React.Component {
  componentDidMount() {
    document.title = "My Nutripal";
  }
  
  render() {
    return (
      <div className="home center">
        <img className="logo" src={logo} />
        <br />
        <span className="header">MyNutriPal</span>
        <p style={{ fontSize: "40px", fontFamily: "Garamond" }}>
          Your best diet-planning friend!
        </p>
      </div>
    );
  }
}

export default HomePage;
