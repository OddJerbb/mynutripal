import React from "react";
import HomePage from "./components/Home/HomePage.jsx";
import MyNavbar from "./components/Navbar/Navbar.jsx";
import AboutPage from "./components/About/AboutPage.jsx";
import Restaurant from "./components/models/Restaurant/Restaurant.jsx";
import Nutrition from "./components/models/Nutrition/Nutrition.jsx";
import Recipe from "./components/models/Recipe/Recipe.jsx";
import "./App.css";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import RestaurantPage from "./components/models/Restaurant/RestaurantPage.jsx";
import RecipePage from "./components/models/Recipe/RecipePage.jsx";
import NutritionPage from "./components/models/Nutrition/NutritionPage.jsx";
import ReRouter from "./components/ReRouter.jsx";
import Visualization from "./components/Visualization/Visualization.jsx";
function App() {
  return (
    <Router>
      <MyNavbar />
      <Switch>
        <Route exact path="/">
          <HomePage />
        </Route>

        <Route exact path="/api/:model" component={ReRouter}></Route>

        <Route exact path="/Home">
          <HomePage />
        </Route>
        <Route exact path="/Visuals">
          <Visualization />
        </Route>
        <Route exact path="/About">
          <AboutPage />
        </Route>

        {/* restaurant */}
        <Route exact path="/Restaurant">
          <Restaurant />
        </Route>
        <Route exact path="/Restaurant/:id" component={RestaurantPage}></Route>

        {/* nutrition */}
        <Route exact path="/Nutrition">
          <Nutrition />
        </Route>
        <Route exact path="/Nutrition/:id" component={NutritionPage}></Route>

        {/* recipe */}
        <Route exact path="/Recipe">
          <Recipe />
        </Route>
        <Route exact path="/Recipe/:id" component={RecipePage}></Route>
      </Switch>
    </Router>
  );
}

export default App;
