// import { render, screen } from "@testing-library/react";
// import App from "./App";

// test('renders learn react link', () => {
//   render(<App />);
//   const linkElement = screen.getByText(/learn react/i);
//   expect(linkElement).toBeInTheDocument();
// });

import React from "react";
import ReactDOM from "react-dom";
// import App from "./App";
import renderer from "react-test-renderer";
import Pagination from "./components/utils/Pagination";
import AboutPage from "./components/About/AboutPage";
import Nutrition from "./components/models/Nutrition/Nutrition";
import NutritionRow from "./components/models/Nutrition/NutritionRow";
// import NutritionPage from "./components/models/Nutrition/NutritionPage";
import RestaurantRow from "./components/models/Restaurant/RestaurantRow";
import Restaurant from "./components/models/Restaurant/Restaurant";

import RecipeRow from "./components/models/Recipe/RecipeRow";
import Recipe from "./components/models/Recipe/Recipe";
//
test("test1", () => {
  const tree = renderer
    .create(
      <Pagination
        dataName={"nutritionalInfo"}
        tHead={[
          {
            displayName: "Name",
            queryName: "name",
            filterOptions: ["rrfrfrrfraw", "3 Muskateers", "beverage"],
          },
          {
            displayName: "Protein",
            queryName: "nutrient_protein",
            filterOptions: ["0", "2.8"],
          },
        ]}
        RenderComponent={NutritionRow}
        title="Nutrition"
        pageLimit={5}
        dataLimit={10}
      />
    )
    .toJSON();
  // console.log(tree);
  expect(tree).toMatchSnapshot();
});

test("test2", () => {
  const tree = renderer
    .create(
      <Pagination
        dataName={"nutritionalInfo"}
        tHead={[
          {
            displayName: "Name",
            queryName: "name",
            filterOptions: ["rarrfrfrw", "3 Muskateers", "beverage"],
          },
          {
            displayName: "Protein",
            queryName: "nutrient_protein",
            filterOptions: ["0", "2.8"],
          },
        ]}
        RenderComponent={NutritionRow}
        title="Nutrition"
        pageLimit={5}
        dataLimit={10}
      />
    )

    .toJSON();
  // console.log(tree);
  expect(tree).toMatchSnapshot();
});

test("test3", () => {
  const tree = renderer
    .create(<AboutPage />)

    .toJSON();
  // console.log(tree);
  expect(tree).toMatchSnapshot();
});

test("test4", () => {
  const tree = renderer
    .create(
      <Pagination
        dataName={"nutritionalInfo"}
        tHead={[
          {
            displayName: "Name",
            queryName: "name",
            filterOptions: ["rarrrfrfrfw", "3 Muskateers", "beverage"],
          },
          {
            displayName: "Protein",
            queryName: "nutrient_protein",
            filterOptions: ["0", "2.8"],
          },
        ]}
        RenderComponent={NutritionRow}
        title="Nutrition"
        pageLimit={5}
        dataLimit={10}
      />
    )

    .toJSON();
  // console.log(tree);
  expect(tree).toMatchSnapshot();
});

test("test5", () => {
  const tree = renderer
    .create(
      <Pagination
        dataName={"nutritionalInfo"}
        tHead={[
          {
            displayName: "Name",
            queryName: "name",
            filterOptions: ["raw", "3 Musfdccdcdkateers", "beverage"],
          },
          {
            displayName: "Protein",
            queryName: "nutrient_protein",
            filterOptions: ["0", "2.8"],
          },
        ]}
        RenderComponent={NutritionRow}
        title="Nutrition"
        pageLimit={5}
        dataLimit={10}
      />
    )

    .toJSON();
  // console.log(tree);
  expect(tree).toMatchSnapshot();
});

test("test6", () => {
  const tree = renderer.create(<Restaurant />).toJSON();
  // console.log(tree);
  expect(tree).toMatchSnapshot();
});

test("test7", () => {
  const tree = renderer
    .create(
      <Pagination
        dataName={"nutritionalInfo"}
        tHead={[
          {
            displayName: "Name",
            queryName: "name",
            filterOptions: ["raefrfrw", "3 Muskateers", "beverage"],
          },
          {
            displayName: "Protein",
            queryName: "nutrient_protein",
            filterOptions: ["0", "2.8"],
          },
        ]}
        RenderComponent={NutritionRow}
        title="Nutrition"
        pageLimit={5}
        dataLimit={10}
      />
    )

    .toJSON();
  // console.log(tree);
  expect(tree).toMatchSnapshot();
});

test("test8", () => {
  const tree = renderer
    .create(
      <Pagination
        dataName={"nutritionalInfo"}
        tHead={[
          {
            displayName: "Name",
            queryName: "name",
            filterOptions: ["rafvfvvfw", "3 Muskateers", "beverage"],
          },
          {
            displayName: "Protein",
            queryName: "nutrient_protein",
            filterOptions: ["0", "2.8"],
          },
        ]}
        RenderComponent={NutritionRow}
        title="Nutrition"
        pageLimit={5}
        dataLimit={10}
      />
    )

    .toJSON();
  // console.log(tree);
  expect(tree).toMatchSnapshot();
});

test("test9", () => {
  const tree = renderer
    .create(
      <Pagination
        dataName={"nutritionalInfo"}
        tHead={[
          {
            displayName: "Name",
            queryName: "name",
            filterOptions: ["rfffaw", "3 Muskateers", "beverage"],
          },
          {
            displayName: "Protein",
            queryName: "nutrient_protein",
            filterOptions: ["0", "2.8"],
          },
        ]}
        RenderComponent={NutritionRow}
        title="Nutrition"
        pageLimit={5}
        dataLimit={10}
      />
    )

    .toJSON();
  // console.log(tree);
  expect(tree).toMatchSnapshot();
});

test("test10", () => {
  const tree = renderer.create(<Recipe />).toJSON();
  // console.log(tree);
  expect(tree).toMatchSnapshot();
});
