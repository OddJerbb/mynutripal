const webdriver = require("selenium-webdriver");
const driver = new webdriver.Builder().forBrowser("firefox").build();
// Instantiate a web browser page

const By = webdriver.By; // useful Locator utility to describe a query for a WebElement
driver
  .navigate()
  .to("https://www.mynutripal.me")
  .then(() => driver.findElement(By.xpath("/html/body/div/div[2]/span")))
  .then((element) => console.log(element));
// .then(value => console.log(value));
//
