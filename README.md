Group members (GitLab ID, EID):

- Benjamin Li, BenjaminGuLi, bgl455
- Harshavardha Jagannathan, harsha31.jagan, hj6987
- Jibran Khalil, Jibran12345, jk45766
- Timothy Qin, timothytqin, ttq77

Git SHA: eaf4c8bd5276de105faecf59f41a23c60827d371

Project Leader: Timothy Qin

GitLab Pipelines: https://gitlab.com/OddJerbb/mynutripal/-/pipelines

Website: https://www.mynutripal.me

Postman documentation: https://documenter.getpostman.com/view/10108309/UVC9h5oP

Estimated completion time for each member:

- Benjamin L - 30 hours
- Harshavardha J - 30 hours
- Jibran K - 30 hours
- Timothy Qin - 34 hours

Actual completion time for each member:

- Benjamin L - 15 hours
- Harshavardha J - 60 hours
- Jibran K - 30 hours
- Timothy Qin - 40 hours

Comments: We learned a lot about front end development and setting up a website through this project.
