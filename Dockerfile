# pull official base image
FROM node:13.12.0-alpine

# set working directory
WORKDIR /app

# add `/app/node_modules/.bin` to $PATH
ENV PATH /app/node_modules/.bin:$PATH

# install app dependencies
COPY package-lock.json ./
COPY front-end/package.json ./
COPY front-end/package-lock.json ./
COPY front-end/public/index.html ./

RUN npm install --silent
RUN npm install react-scripts@3.4.1 -g --silent

# add app
COPY front-end .

# start app
CMD ["yarn", "start"]
