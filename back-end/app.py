import json
import flask

# from flask_cors import CORS
import sqlalchemy as db
from sqlalchemy.sql import text

app = flask.Flask(__name__)
# cors = CORS(app)

engine = db.create_engine(
    "mysql+pymysql://admin:mynutripal@mnp.cgimo2xbdvux.us-east-2.rds.amazonaws.com:3306/mnp"
)
connection = engine.connect()

# define filter, sort, and searchable columns and types
tables = {
    "MNP_Recipes": {
        "id": "str",
        "name": "str",
        "difficulty": "num",
        "category": "str",
        "origin": "str",
        "instructions": "str",
        "tags": "str",
    },
    "MNP_NutritionalInfos": {
        "id": "str",
        "name": "str",
        "nutrient_protein": "num",
        "nutrient_fat": "num",
        "nutrient_carb": "num",
        "nutrient_energy": "num",
    },
    "MNP_Restaurants": {
        "id": "str",
        "restaurant_name": "str",
        "restaurant_phone": "str",
        "restaurant_website": "str",
        "hours": "str",
        "rating": "num",
        "avg_price": "num",
        "price_range": "str",
        "address": "str",
        "distance": "num",
    },
}


def query(table_name, params):

    # extract special query params
    query = f"SELECT * FROM {table_name} "
    page = int(params.get("page")) if params.get("page") else 0
    page_size = int(params.get("pageSize")) if params.get("pageSize") else 10
    sort_by = params.get("sortBy") if params.get("sortBy") else None
    sort_order = (
        "DESC"
        if params.get("sortOrder") and params.get("sortOrder") == "DESC"
        else "ASC"
    )
    search_query = params.get("search")

    # get table-specific filters
    filters = list(
        set(list(params.keys()))
        - set(["search", "pageSize", "page", "sortBy", "sort_order"])
    )

    # match search query against all columns
    if search_query:
        query += "WHERE ("

        cols = tables[table_name].keys()

        for ind, col_name in enumerate(cols):
            query += f"""{col_name} LIKE '%{search_query}%' {"OR " if ind < len(cols) - 1 else ""}"""

        query += ") "

    # filter on results
    if len(filters) > 0:
        query += "WHERE (" if not search_query else "AND ("

        for ind, filter in enumerate(filters):
            filter_type = tables[table_name][filter]

            # columns with type num usually take range
            # url query param formats: COL_NAME=MIN,MAX [MIN, MAX]
            #                          COL_NAME=MIN,    [MIN, 1000000]
            #                          COL_NAME=,MAX    [0, MAX]
            #                          COL_NAME=VAL     [VAL]
            if filter_type == "num":
                if "," in params.get(filter):
                    bounds = params.get(filter).split(",")
                    left_bound = float(bounds[0]) if bounds[0] else 0
                    right_bound = float(bounds[1]) if bounds[1] else 1000000
                    query += f"""{filter} BETWEEN {left_bound} AND {right_bound} """
                else:
                    query += f"""{filter}={params.get(filter)} """

            else:
                # id=SOME_ID [1, 200]
                val = (
                    params.get(filter)
                    if filter == "id"
                    else f"""'{params.get(filter)}'"""
                )

                query += f"""{filter}={val} """

            query += f"""{"AND " if ind < len(filters) - 1 else ""}"""

        query += ") "

    if sort_by:
        query += f"""ORDER BY {sort_by} {sort_order} """

    print(query)

    ResultProxy = connection.execute(text(query))
    res = [dict(r) for r in ResultProxy]
    offset = page * page_size

    return json.dumps(
        {"num_results": len(res), "data": res[offset : offset + page_size]}
    )


def getResults(table_name, args):
    args = flask.request.args
    result = query(table_name, args)
    return flask.jsonify(str(result))


@app.route("/recipes")
def get_recipes():
    return getResults("MNP_Recipes", flask.request.args)


@app.route("/nutritionalInfo")
def get_nutritional_info():
    return getResults("MNP_NutritionalInfos", flask.request.args)


@app.route("/restaurants")
def get_restaurants():
    return getResults("MNP_Restaurants", flask.request.args)
