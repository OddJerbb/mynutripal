create table MNP_Restaurants (
   id INT NOT NULL AUTO_INCREMENT,
   restaurant_name VARCHAR(100) NOT NULL,
   restaurant_phone VARCHAR(12),
   restaurant_website VARCHAR(100),
   hours VARCHAR(100),
   rating INT,
   avg_price FLOAT,
   price_range VARCHAR(100),
   address VARCHAR(100),
   geo_lat FLOAT,
   geo_lon FLOAT,
   distance FLOAT,
   PRIMARY KEY ( id )
);

create table MNP_NutritionalInfos (
   id INT NOT NULL AUTO_INCREMENT,
   name VARCHAR(100) NOT NULL,
   nutrient_protein VARCHAR(100),
   nutrient_fat VARCHAR(100),
   nutrient_carb VARCHAR(100),
   nutrient_energy VARCHAR(100),
   PRIMARY KEY ( id )
);

create table MNP_Recipes (
   id INT NOT NULL AUTO_INCREMENT,
   name VARCHAR(100) NOT NULL,
   difficulty INT,
   category VARCHAR(100),
   origin VARCHAR(100),
   thumbnail VARCHAR(100),
   instructions VARCHAR(5000),
   tags VARCHAR(100),
   youtube_url VARCHAR(100),
   PRIMARY KEY ( id )
);
