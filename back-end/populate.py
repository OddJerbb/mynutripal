import time
import json
import flask

# from flask_cors import CORS
import requests
import sqlalchemy as db
from sqlalchemy import Column, Integer, String, Float
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

app = flask.Flask(__name__)
# cors = CORS(app)

engine = db.create_engine(
    "mysql+pymysql://admin:mynutripal@mnp.cgimo2xbdvux.us-east-2.rds.amazonaws.com:3306/mnp"
)
connection = engine.connect()
metadata = db.MetaData()

Session = sessionmaker(bind=engine)
session = Session()

recipes = db.Table("Recipe", metadata, autoload=True, autoload_with=engine)
nutri_info = db.Table("NutritionalInfos", metadata, autoload=True, autoload_with=engine)
restaurants = db.Table("Restaurant", metadata, autoload=True, autoload_with=engine)

themealdb_api_key = "1"
usda_api_key = "pHKrA1WTZlsfbwAIeOxKG88Q0Lea8EkZ4IBAWNsn"
documenu_api_key = "8cc6bc62a8a76e38cb3b6f4b7638d613"


def get_records_from_data_source(url, api_key):
    res_per_page = 200
    num_records = 1000
    records = []
    for i in range(num_records // res_per_page):
        page = requests.get(url.format(i + 1, api_key)).json()
        print(page)
        for record in page if type(page) is list else page["data"]:
            records.append(record)
    return records


@app.route("/populate/recipes")
def themealdb():
    records = []
    for i in range(200):
        res = requests.get(
            f"https://www.themealdb.com/api/json/v1/{themealdb_api_key}/random.php"
        ).json()["meals"][0]
        records.append(
            {
                "name": res["strMeal"],
                "category": res["strCategory"],
                "origin": res["strArea"],
                "thumbnail": res["strMealThumb"],
                "instructions": res["strInstructions"],
                "tags": res["strTags"],
                "youtube_url": res["strYoutube"],
            }
        )

    query = db.insert(recipes)
    ResultProxy = connection.execute(query, records)

    return flask.jsonify(records)


def search_for_nutrient_number(record, n):
    for i in range(len(record)):
        if record[i]["number"] == str(n):
            return record[i]
    return None


def build_nutrient_str(record):
    return (
        " ".join(
            [
                str(record["amount"] if record else 0),
                record["unitName"] if record else "mg",
            ]
        ),
    )


def process_usda(r):
    return {
        "name": r["description"],
        "nutrient_protein": build_nutrient_str(
            search_for_nutrient_number(r["foodNutrients"], 203)
        ),
        "nutrient_fat": build_nutrient_str(
            search_for_nutrient_number(r["foodNutrients"], 204)
        ),
        "nutrient_carb": build_nutrient_str(
            search_for_nutrient_number(r["foodNutrients"], 205)
        ),
        "nutrient_energy": build_nutrient_str(
            search_for_nutrient_number(r["foodNutrients"], 208)
        ),
    }


@app.route("/populate/nutriinfo")
def usda():
    records = get_records_from_data_source(
        "https://api.nal.usda.gov/fdc/v1/foods/list?pageSize=25&pageNumber={}&api_key={}",
        usda_api_key,
    )

    res = list(map(process_usda, records))

    query = db.insert(nutri_info)
    ResultProxy = connection.execute(query, res)

    return flask.jsonify(res)


@app.route("/populate/restaurants")
def documenu():
    records = get_records_from_data_source(
        "https://api.documenu.com/v2/restaurants/zip_code/78705?size=200&page={}&key={}",
        documenu_api_key,
    )

    print(records)

    res = list(
        map(
            lambda r: {
                "restaurant_name": r["restaurant_name"],
                "restaurant_phone": r["restaurant_phone"],
                "restaurant_website": r["restaurant_website"],
                "hours": r["hours"],
                "price_range": r["price_range"],
                "address": r["address"]["formatted"],
                "geo_lat": r["geo"]["lat"],
                "geo_lon": r["geo"]["lon"],
            },
            records,
        )
    )

    print(res)

    query = db.insert(restaurants)
    ResultProxy = connection.execute(query, res)

    return flask.jsonify(res)
