import unittest
from unittest import main, TestCase
import requests


class UnitTest(TestCase):
    def test_restaurant(self):
        endpoint = "https://r1muet4wvk.execute-api.us-east-2.amazonaws.com/dev/restaurants?page=0"
        r = requests.get(endpoint)
        self.assertEqual(200, r.status_code)

    def test_recipe(self):
        endpoint = (
            "https://r1muet4wvk.execute-api.us-east-2.amazonaws.com/dev/recipes?page=0"
        )
        r = requests.get(endpoint)
        self.assertEqual(200, r.status_code)

    def test_nutritionalInfo(self):
        endpoint = "https://r1muet4wvk.execute-api.us-east-2.amazonaws.com/dev/nutritionalInfo?page=0"
        r = requests.get(endpoint)
        self.assertEqual(200, r.status_code)

if __name__ == '__main__':
    unittest.main()