INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(001, '100 GRAND Bar', '2.5 G', '19.3 G', '71.0 G', '468 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(002, '3 MUSKETEERS Bar', '2.6 G', '12.8 G', '77.8 G', '436 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(003, '3 Musketeers Truffle Crisp Bar', '6.41 G', '28.8 G', '63.2 G', '538 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(004, 'Abalone, cooked, NS as to cooking method', '20.4 G', '4.59 G', '7.26 G', '158 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(005, 'Abalone, floured or breaded, fried', '18.2 G', '11.4 G', '15.5 G', '242 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(006, 'Abalone, steamed or poached', '34.0 G', '1.51 G', '12.0 G', '209 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(007, 'Abiyuch, raw', '1.5 G', '0.1 G', '17.6 G', '69.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(008, 'Acerola juice, raw', '0.4 G', '0.3 G', '4.8 G', '23.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(009, 'Acerola, (west indian cherry), raw', '0.4 G', '0.3 G', '7.69 G', '32.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(010, 'Acorn stew (Apache)', '6.81 G', '3.47 G', '9.22 G', '95.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(011, 'Adobo, with noodles', '16.9 G', '7.66 G', '8.16 G', '172 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(012, 'Adobo, with rice', '17.7 G', '7.94 G', '8.88 G', '181 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(013, 'Agave liquid sweetener', '0.09 G', '0.45 G', '76.4 G', '310 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(014, 'Agave, cooked (Southwest)', '0.99 G', '0.29 G', '32.0 G', '135 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(015, 'Agave, dried (Southwest)', '1.71 G', '0.69 G', '82.0 G', '341 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(016, 'Agave, raw (Southwest)', '0.52 G', '0.15 G', '16.2 G', '68.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(017, 'Agutuk, fish with shortening (Alaskan ice cream) (Alaska Native)', '9.0 G', '43.5 G', '10.5 G', '470 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(018, 'Agutuk, fish/berry with seal oil (Alaskan ice cream) (Alaska Native)', '3.4 G', '31.8 G', '13.4 G', '353 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(019, 'Agutuk, meat-caribou (Alaskan ice cream) (Alaska Native)', '21.7 G', '18.6 G', '0.9 G', '258 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(020, 'Air filled fritter or fried puff, without syrup, Puerto Rican style', '6.46 G', '23.8 G', '50.2 G', '435 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(021, 'Alcoholic beverage, beer, light', '0.24 G', '0.0 G', '1.64 G', '29.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(022, 'Alcoholic beverage, beer, light, BUD LIGHT', '0.25 G', '0.0 G', '1.3 G', '29.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(023, 'Alcoholic beverage, beer, light, BUDWEISER SELECT', '0.2 G', '0.0 G', '0.87 G', '28.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(024, 'Alcoholic beverage, beer, light, higher alcohol', '0.25 G', '0.0 G', '0.77 G', '46.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(025, 'Alcoholic beverage, beer, light, low carb', '0.17 G', '0.0 G', '0.73 G', '27.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(026, 'Alcoholic beverage, beer, regular, all', '0.46 G', '0.0 G', '3.55 G', '43.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(027, 'Alcoholic beverage, beer, regular, BUDWEISER', '0.36 G', '0.0 G', '2.97 G', '41.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(028, 'Alcoholic beverage, creme de menthe, 72 proof', '0.0 G', '0.3 G', '41.6 G', '371 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(029, 'Alcoholic beverage, daiquiri, canned', '0.0 G', '0.0 G', '15.7 G', '125 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(030, 'Alcoholic beverage, daiquiri, prepared-from-recipe', '0.06 G', '0.06 G', '6.94 G', '186 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(031, 'Alcoholic beverage, distilled, all (gin, rum, vodka, whiskey) 100 proof', '0.0 G', '0.0 G', '0.0 G', '295 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(032, 'Alcoholic beverage, distilled, all (gin, rum, vodka, whiskey) 80 proof', '0.0 G', '0.0 G', '0.0 G', '231 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(033, 'Alcoholic beverage, distilled, all (gin, rum, vodka, whiskey) 86 proof', '0.0 G', '0.0 G', '0.1 G', '250 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(034, 'Alcoholic beverage, distilled, all (gin, rum, vodka, whiskey) 90 proof', '0.0 G', '0.0 G', '0.0 G', '263 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(035, 'Alcoholic beverage, distilled, all (gin, rum, vodka, whiskey) 94 proof', '0.0 G', '0.0 G', '0.0 G', '275 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(036, 'Alcoholic beverage, distilled, rum, 80 proof', '0.0 G', '0.0 G', '0.0 G', '231 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(037, 'Alcoholic beverage, distilled, vodka, 80 proof', '0.0 G', '0.0 G', '0.0 G', '231 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(038, 'Alcoholic beverage, distilled, whiskey, 86 proof', '0.0 G', '0.0 G', '0.1 G', '250 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(039, 'Alcoholic beverage, liqueur, coffee with cream, 34 proof', '2.8 G', '15.7 G', '20.9 G', '327 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(040, 'Alcoholic beverage, liqueur, coffee, 53 proof', '0.1 G', '0.3 G', '46.8 G', '336 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(041, 'Alcoholic beverage, liqueur, coffee, 63 proof', '0.1 G', '0.3 G', '32.2 G', '308 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(042, 'Alcoholic beverage, malt beer, hard lemonade', '0.0 G', '0.0 G', '10.1 G', '68.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(043, 'Alcoholic beverage, pina colada, canned', '0.6 G', '7.6 G', '27.6 G', '237 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(044, 'Alcoholic beverage, pina colada, prepared-from-recipe', '0.42 G', '1.88 G', '22.7 G', '174 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(045, 'Alcoholic beverage, rice (sake)', '0.5 G', '0.0 G', '5.0 G', '134 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(046, 'Alcoholic beverage, tequila sunrise, canned', '0.3 G', '0.1 G', '11.3 G', '110 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(047, 'Alcoholic beverage, whiskey sour', '0.0 G', '0.03 G', '13.2 G', '149 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(048, 'Alcoholic beverage, whiskey sour, canned', '0.0 G', '0.0 G', '13.4 G', '119 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(049, 'Alcoholic beverage, whiskey sour, prepared from item 14028', '0.06 G', '0.06 G', '12.8 G', '153 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(050, 'Alcoholic beverage, whiskey sour, prepared with water, whiskey and powder mix', '0.1 G', '0.02 G', '15.8 G', '164 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(051, 'Alcoholic beverage, wine, cooking', '0.5 G', '0.0 G', '6.3 G', '50.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(052, 'Alcoholic beverage, wine, dessert, dry', '0.2 G', '0.0 G', '11.7 G', '152 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(053, 'Alcoholic beverage, wine, dessert, sweet', '0.2 G', '0.0 G', '13.7 G', '160 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(054, 'Alcoholic beverage, wine, light', '0.07 G', '0.0 G', '1.17 G', '49.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(055, 'Alcoholic beverage, wine, table, all', '0.07 G', '0.0 G', '2.72 G', '83.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(056, 'Alcoholic beverage, wine, table, red', '0.07 G', '0.0 G', '2.61 G', '85.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(057, 'Alcoholic Beverage, wine, table, red, Barbera', '0.07 G', '0.0 G', '2.79 G', '85.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(058, 'Alcoholic Beverage, wine, table, red, Burgundy', '0.07 G', '0.0 G', '3.69 G', '86.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(059, 'Alcoholic Beverage, wine, table, red, Cabernet Franc', '0.07 G', '0.0 G', '2.45 G', '83.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(060, 'Alcoholic Beverage, wine, table, red, Cabernet Sauvignon', '0.07 G', '0.0 G', '2.6 G', '83.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(061, 'Alcoholic Beverage, wine, table, red, Carignane', '0.07 G', '0.0 G', '2.4 G', '74.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(062, 'Alcoholic Beverage, wine, table, red, Claret', '0.07 G', '0.0 G', '3.01 G', '83.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(063, 'Alcoholic Beverage, wine, table, red, Gamay', '0.07 G', '0.0 G', '2.38 G', '78.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(064, 'Alcoholic Beverage, wine, table, red, Lemberger', '0.07 G', '0.0 G', '2.46 G', '80.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(065, 'Alcoholic Beverage, wine, table, red, Merlot', '0.07 G', '0.0 G', '2.51 G', '83.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(066, 'Alcoholic Beverage, wine, table, red, Mouvedre', '0.07 G', '0.0 G', '2.64 G', '88.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(067, 'Alcoholic Beverage, wine, table, red, Petite Sirah', '0.07 G', '0.0 G', '2.68 G', '85.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(068, 'Alcoholic Beverage, wine, table, red, Pinot Noir', '0.07 G', '0.0 G', '2.31 G', '82.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(069, 'Alcoholic Beverage, wine, table, red, Sangiovese', '0.07 G', '0.0 G', '2.62 G', '86.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(070, 'Alcoholic Beverage, wine, table, red, Syrah', '0.07 G', '0.0 G', '2.58 G', '83.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(071, 'Alcoholic Beverage, wine, table, red, Zinfandel', '0.07 G', '0.0 G', '2.86 G', '88.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(072, 'Alcoholic beverage, wine, table, white', '0.07 G', '0.0 G', '2.6 G', '82.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(073, 'Alcoholic beverage, wine, table, white, Chardonnay', '0.07 G', '0.0 G', '2.16 G', '84.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(074, 'Alcoholic beverage, wine, table, white, Chenin Blanc', '0.07 G', '0.0 G', '3.31 G', '80.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(075, 'Alcoholic beverage, wine, table, white, Fume Blanc', '0.07 G', '0.0 G', '2.27 G', '82.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(076, 'Alcoholic beverage, wine, table, white, Gewurztraminer', '0.07 G', '0.0 G', '2.6 G', '81.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(077, 'Alcoholic beverage, wine, table, white, late harvest', '0.07 G', '0.0 G', '13.4 G', '112 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(078, 'Alcoholic beverage, wine, table, white, late harvest, Gewurztraminer', '0.07 G', '0.0 G', '11.4 G', '108 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(079, 'Alcoholic beverage, wine, table, white, Muller Thurgau', '0.07 G', '0.0 G', '3.48 G', '76.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(080, 'Alcoholic beverage, wine, table, white, Muscat', '0.07 G', '0.0 G', '5.23 G', '82.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(081, 'Alcoholic beverage, wine, table, white, Pinot Blanc', '0.07 G', '0.0 G', '1.94 G', '81.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(082, 'Alcoholic beverage, wine, table, white, Pinot Gris (Grigio)', '0.07 G', '0.0 G', '2.06 G', '83.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(083, 'Alcoholic beverage, wine, table, white, Riesling', '0.07 G', '0.0 G', '3.74 G', '80.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(084, 'Alcoholic beverage, wine, table, white, Sauvignon Blanc', '0.07 G', '0.0 G', '2.05 G', '81.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(085, 'Alcoholic beverage, wine, table, white, Semillon', '0.07 G', '0.0 G', '3.12 G', '82.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(086, 'Alcoholic beverages, beer, higher alcohol', '0.9 G', '0.0 G', '0.27 G', '58.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(087, 'Alcoholic beverages, wine, rose', '0.36 G', '0.0 G', '3.8 G', '83.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(088, 'Alcoholic malt beverage', '0.9 G', '0.0 G', '0.27 G', '58.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(089, 'Alcoholic malt beverage, higher alcohol, sweetened', '0.0 G', '0.0 G', '8.86 G', '88.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(090, 'Alcoholic malt beverage, sweetened', '0.0 G', '0.0 G', '10.1 G', '68.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(091, 'Alexander', '1.05 G', '3.83 G', '18.9 G', '238 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(092, 'Alfalfa seeds, sprouted, raw', '3.99 G', '0.69 G', '2.1 G', '23.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(093, 'Alfalfa sprouts, raw', '3.99 G', '0.69 G', '2.1 G', '23.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(094, 'Alfredo sauce', '2.37 G', '15.1 G', '1.48 G', '149 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(095, 'Alfredo sauce with added vegetables', '2.28 G', '11.6 G', '2.32 G', '120 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(096, 'Alfredo sauce with meat', '6.72 G', '15.4 G', '1.2 G', '170 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(097, 'Alfredo sauce with meat and added vegetables', '6.64 G', '12.6 G', '1.88 G', '146 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(098, 'Alfredo sauce with poultry', '7.1 G', '13.6 G', '1.2 G', '153 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(099, 'Alfredo sauce with poultry and added vegetables', '7.03 G', '10.8 G', '1.88 G', '130 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(100, 'Alfredo sauce with seafood', '6.11 G', '12.6 G', '1.76 G', '144 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(101, 'Alfredo sauce with seafood and added vegetables', '6.04 G', '9.74 G', '2.43 G', '120 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(102, 'Almond butter', '21.0 G', '55.5 G', '18.8 G', '614 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(103, 'Almond butter, lower sodium', '21.0 G', '55.5 G', '18.8 G', '614 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(104, 'Almond chicken', '16.1 G', '13.3 G', '4.69 G', '197 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(105, 'Almond milk, sweetened', '0.38 G', '0.93 G', '5.24 G', '30.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(106, 'Almond milk, sweetened, chocolate', '0.43 G', '0.95 G', '8.34 G', '41.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(107, 'Almond milk, unsweetened', '0.4 G', '0.96 G', '1.31 G', '15.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(108, 'Almond milk, unsweetened, chocolate', '0.46 G', '1.0 G', '1.48 G', '16.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(109, 'Almond milk, unsweetened, plain, shelf stable', '0.555 G', '1.22 G', '0.337 G', '0 mg');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(110, 'Almond oil', '0.0 G', '100 G', '0.0 G', '884 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(111, 'Almond paste', '9.0 G', '27.7 G', '47.8 G', '458 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(112, 'Almonds, chocolate covered', '17.1 G', '43.8 G', '32.9 G', '566 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(113, 'Almonds, flavored', '20.1 G', '53.3 G', '20.1 G', '599 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(114, 'Almonds, honey roasted', '14.1 G', '45.3 G', '35.8 G', '574 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(115, 'Almonds, lightly salted', '20.2 G', '53.7 G', '20.3 G', '604 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(116, 'Almonds, NFS', '21.0 G', '52.5 G', '21.0 G', '598 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(117, 'Almonds, salted', '20.1 G', '53.4 G', '20.2 G', '601 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(118, 'Almonds, sugar-coated', '10.0 G', '17.9 G', '68.3 G', '465 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(119, 'Almonds, unroasted', '21.2 G', '49.9 G', '21.6 G', '579 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(120, 'Almonds, unsalted', '20.3 G', '54.0 G', '20.4 G', '607 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(121, 'Almonds, yogurt-covered', '12.8 G', '37.3 G', '44.9 G', '548 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(122, 'Aloe vera juice drink', '0.0 G', '0.0 G', '3.75 G', '15.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(123, 'Amaranth grain, cooked', '3.8 G', '1.58 G', '18.7 G', '102 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(124, 'Amaranth grain, uncooked', '13.6 G', '7.02 G', '65.2 G', '371 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(125, 'Amaranth leaves, cooked, boiled, drained, with salt', '2.11 G', '0.18 G', '4.11 G', '21.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(126, 'Amaranth leaves, cooked, boiled, drained, without salt', '2.11 G', '0.18 G', '4.11 G', '21.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(127, 'Amaranth leaves, raw', '2.46 G', '0.33 G', '4.02 G', '23.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(128, 'Ambrosia', '0.9 G', '0.96 G', '16.2 G', '70.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(129, 'Anchovy, canned', '28.9 G', '9.71 G', '0.0 G', '210 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(130, 'Anchovy, cooked, NS as to cooking method', '28.9 G', '9.71 G', '0.0 G', '210 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(131, 'Andrea''s, Gluten Free Soft Dinner Roll', '5.65 G', '8.2 G', '40.2 G', '257 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(132, 'Animal fat or drippings', '2.23 G', '92.2 G', '0.0 G', '841 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(133, 'Animal fat, bacon grease', '0.0 G', '99.5 G', '0.0 G', '897 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(134, 'Anisette toast', '10.1 G', '9.7 G', '74.2 G', '426 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(135, 'Antipasto with ham, fish, cheese, vegetables', '11.6 G', '10.7 G', '3.88 G', '158 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(136, 'Apple cider', '0.1 G', '0.13 G', '11.3 G', '46.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(137, 'Apple juice beverage, 40-50% juice, light', '0.0 G', '0.1 G', '5.1 G', '22.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(138, 'Apple juice, 100%', '0.1 G', '0.13 G', '11.3 G', '46.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(139, 'Apple juice, 100%, with calcium added', '0.12 G', '0.17 G', '11.5 G', '48.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(140, 'Apple juice, baby food', '0.0 G', '0.1 G', '11.7 G', '47.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(141, 'Apple juice, canned or bottled, unsweetened, with added ascorbic acid', '0.1 G', '0.13 G', '11.3 G', '46.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(142, 'Apple juice, canned or bottled, unsweetened, with added ascorbic acid, calcium, and potassium', '0.12 G', '0.17 G', '11.5 G', '48.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(143, 'Apple juice, canned or bottled, unsweetened, without added ascorbic acid', '0.1 G', '0.13 G', '11.3 G', '46.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(144, 'Apple juice, frozen concentrate, unsweetened, diluted with 3 volume water without added ascorbic aci', '0.14 G', '0.1 G', '11.5 G', '47.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(145, 'Apple juice, frozen concentrate, unsweetened, diluted with 3 volume water, with added ascorbic acid', '0.14 G', '0.1 G', '11.5 G', '47.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(146, 'Apple juice, frozen concentrate, unsweetened, undiluted, with added ascorbic acid', '0.51 G', '0.37 G', '41.0 G', '166 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(147, 'Apple juice, frozen concentrate, unsweetened, undiluted, without added ascorbic acid', '0.51 G', '0.37 G', '41.0 G', '166 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(148, 'Apple juice, with added calcium, baby food', '0.06 G', '0.1 G', '11.1 G', '46.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(149, 'Apple juice, with added vitamin C, from concentrate, shelf stable', '0.0859 G', '0.286 G', '11.4 G', '0 mg');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(150, 'Apple pie filling', '0.1 G', '0.1 G', '26.1 G', '100 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(151, 'Apple salad with dressing', '1.48 G', '13.9 G', '12.2 G', '172 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(152, 'Apple yogurt dessert, baby food, strained', '0.8 G', '1.6 G', '19.5 G', '93.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(153, 'Apple, baked', '0.32 G', '3.04 G', '22.7 G', '112 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(154, 'Apple, candied', '1.34 G', '2.15 G', '29.6 G', '134 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(155, 'Apple, dried', '0.93 G', '0.32 G', '65.9 G', '243 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(156, 'Apple, raw', '0.26 G', '0.17 G', '13.8 G', '52.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(157, 'Apple-banana juice, baby food', '0.2 G', '0.1 G', '12.3 G', '51.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(158, 'Apple-cherry juice, baby food', '0.2 G', '0.1 G', '11.2 G', '47.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(159, 'Apple-fruit juice blend, baby food', '0.2 G', '0.1 G', '18.1 G', '72.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(160, 'Apple-grape juice, baby food', '0.1 G', '0.2 G', '11.3 G', '46.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(161, 'Apple-peach juice, baby food', '0.2 G', '0.1 G', '10.5 G', '43.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(162, 'Apple-prune juice, baby food', '0.2 G', '0.1 G', '18.1 G', '72.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(163, 'Apple-raspberry, baby food, junior', '0.2 G', '0.2 G', '16.0 G', '60.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(164, 'Apple-raspberry, baby food, NS as to strained or junior', '0.2 G', '0.2 G', '16.0 G', '60.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(165, 'Apple-raspberry, baby food, strained', '0.2 G', '0.2 G', '15.6 G', '58.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(166, 'Apple-sweet potato juice, baby food', '0.3 G', '0.1 G', '11.4 G', '47.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(167, 'APPLEBEE''S, 9 oz house sirloin steak', '26.8 G', '9.08 G', '0.0 G', '189 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(168, 'APPLEBEE''S, chicken tenders platter', '19.6 G', '16.2 G', '18.0 G', '297 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(169, 'APPLEBEE''S, chicken tenders, from kids'' menu', '19.2 G', '16.2 G', '18.4 G', '296 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(170, 'APPLEBEE''S, chili', '12.6 G', '9.79 G', '4.57 G', '157 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(171, 'APPLEBEE''S, coleslaw', '0.79 G', '7.09 G', '13.2 G', '120 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(172, 'APPLEBEE''S, crunchy onion rings', '4.58 G', '19.6 G', '40.2 G', '356 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(173, 'APPLEBEE''S, Double Crunch Shrimp', '12.3 G', '18.9 G', '26.0 G', '323 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(174, 'APPLEBEE''S, fish, hand battered', '13.2 G', '9.14 G', '16.6 G', '202 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(175, 'APPLEBEE''S, french fries', '3.31 G', '13.2 G', '39.5 G', '290 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(176, 'APPLEBEE''S, KRAFT, Macaroni & Cheese, from kid''s menu', '5.01 G', '4.34 G', '21.1 G', '143 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(177, 'APPLEBEE''S, mozzarella sticks', '14.9 G', '18.4 G', '22.9 G', '316 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(178, 'Apples and chicken, baby food, strained', '2.16 G', '1.38 G', '10.9 G', '65.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(179, 'Apples and pears, baby food, junior', '0.15 G', '0.05 G', '11.0 G', '40.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(180, 'Apples and pears, baby food, NS as to strained or junior', '0.2 G', '0.12 G', '10.9 G', '41.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(181, 'Apples and pears, baby food, strained', '0.25 G', '0.2 G', '10.8 G', '42.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(182, 'Apples and sweet potatoes, baby food, strained', '0.3 G', '0.22 G', '15.3 G', '64.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(183, 'Apples with ham, baby food, strained', '2.6 G', '0.9 G', '10.9 G', '62.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(184, 'Apples, baby food, toddler', '0.19 G', '0.36 G', '11.6 G', '50.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(185, 'Apples, canned, sweetened, sliced, drained, heated', '0.18 G', '0.43 G', '16.8 G', '67.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(186, 'Apples, dehydrated (low moisture), sulfured, stewed', '0.28 G', '0.12 G', '19.9 G', '74.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(187, 'Apples, dehydrated (low moisture), sulfured, uncooked', '1.32 G', '0.58 G', '93.5 G', '346 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(188, 'Apples, dried, sulfured, stewed, with added sugar', '0.2 G', '0.07 G', '20.7 G', '83.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(189, 'Apples, dried, sulfured, stewed, without added sugar', '0.22 G', '0.07 G', '15.3 G', '57.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(190, 'Apples, dried, sulfured, uncooked', '0.93 G', '0.32 G', '65.9 G', '243 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(191, 'Apples, frozen, unsweetened, heated (Includes foods for USDA''s Food Distribution Program)', '0.29 G', '0.33 G', '12.0 G', '47.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(192, 'Apples, frozen, unsweetened, unheated (Includes foods for USDA''s Food Distribution Program)', '0.28 G', '0.32 G', '12.3 G', '48.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(193, 'Apples, fuji, with skin, raw', '0.148 G', '0.162 G', '15.7 G', '0 mg');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(194, 'Apples, gala, with skin, raw', '0.133 G', '0.15 G', '14.8 G', '0 mg');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(195, 'Apples, granny smith, with skin, raw', '0.266 G', '0.138 G', '14.1 G', '0 mg');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(196, 'Apples, honeycrisp, with skin, raw', '0.102 G', '0.1 G', '14.7 G', '0 mg');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(197, 'Apples, raw, fuji, with skin (Includes foods for USDA''s Food Distribution Program)', '0.2 G', '0.18 G', '15.2 G', '63.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(198, 'Apples, raw, gala, with skin (Includes foods for USDA''s Food Distribution Program)', '0.25 G', '0.12 G', '13.7 G', '57.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(199, 'Apples, raw, golden delicious, with skin', '0.28 G', '0.15 G', '13.6 G', '57.0 KCAL');
INSERT INTO mnp.MNP_NutritionalInfos
(id, name, nutrient_protein, nutrient_fat, nutrient_carb, nutrient_energy)
VALUES(200, 'Apples, raw, granny smith, with skin (Includes foods for USDA''s Food Distribution Program)', '0.44 G', '0.19 G', '13.6 G', '58.0 KCAL');
