import unittest
from selenium import webdriver
# from selenium import FirefoxOptions
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
class SearchText(unittest.TestCase):
    def setUp(self):
        # create a new Firefox session
        # foptions = FirefoxOptions()
        # foptions.add_argument()
        chrome_options = Options()
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--disable-dev-shm-usage')
        # self.base_url = "http://localhost:3000"
        self.base_url = "https://www.mynutripal.me"
        # cls.driver = webdriver.Chrome('./chromedriver', options=chrome_options)
        # cls.driver.implicitly_wait(10)

        # cls.wait = WebDriverWait(driver, 10)

        
        
        self.driver = webdriver.Chrome("./chromedriver", options=chrome_options)
        self.driver.implicitly_wait(10)
        # self.driver.maximize_window()
        # navigate to the application home page
        # self.wait = WebDriverWait(self.driver, 10)
        self.driver.get(self.base_url)

    def test1(self):
        # get the search textbox
        self.search_field = self.driver.find_element(By.XPATH, '//*[@id="root"]/div[2]/span')
     
        self.assertEqual(self.search_field.text, "MyNutriPal")

    def test2(self):
        # get the search textbox
        self.search_field = self.driver.find_element_by_xpath('//*[@id="root"]/div[2]/p')
     
        self.assertEqual(self.search_field.text, "Your best diet-planning friend!")

    def test3(self):
        # get the search textbox
        self.driver.get(self.base_url+"/restaurant")
        self.search_field = self.driver.find_element_by_xpath('/html/body/div/div[2]/div/h6')

        self.assertTrue(self.search_field.text)
        # if(self.search_field.text):
        #     self.assertTrue()
        # else:
        #     self.assertFalse()

    def test4(self):
        # get the search textbox
        self.driver.get(self.base_url + "/nutrition")
        self.search_field = self.driver.find_element_by_xpath('/html/body/div[1]/div[2]/div/h1')
     
        self.assertTrue(self.search_field.text)
        # if(self.search_field.text):
        #     self.assertTrue(True)
        # else:
        #     self.assertTrue(True)

    def test5(self):
        # get the search textbox
        self.driver.get(self.base_url+"/recipe")
        self.search_field = self.driver.find_element_by_xpath('/html/body/div/div[2]/div/h1')
     
        self.assertTrue(self.search_field.text)
        # if(self.search_field.text):
        #     self.assertTrue(True)
        # else:
        #     self.assertTrue(True)
        # self.assertEqual(self.search_field.text, "Phone number")

    # def test6(self):
    #     # get the search textbox
    #     self.search_field = self.driver.find_element_by_xpath('//*[@id="basic-navbar-nav"]/div/div/div/a[1]')
    
    #     self.assertEqual(self.search_field.text, "Restaurants")

    # def test7(self):
    #     # get the search textbox
    #     self.search_field = self.driver.find_element_by_xpath('//*[@id="basic-navbar-nav"]/div/div/div/a[2]')
    
    #     self.assertEqual(self.search_field.text, "Nutrition")


    # def test8(self):
    #     # get the search textbox
    #     self.search_field = self.driver.find_element_by_xpath('//*[@id="basic-navbar-nav"]/div/div/div/a[3]')
    
    #     self.assertEqual(self.search_field.text, "Recipes")

    def test6(self):
            # get the search textbox
            self.search_field = self.driver.find_element_by_xpath('//*[@id="root"]/div[2]/p')
        
            self.assertEqual(self.search_field.text, "Your best diet-planning friend!")

    def test7(self):
            # get the search textbox
            self.driver.get(self.base_url+"/restaurant")
            self.search_field = self.driver.find_element_by_xpath('/html/body/div/div[2]/div/h6')
        
            self.assertTrue(self.search_field.text)
            # if(self.search_field.text):
            #     self.assertTrue()
            # else:
            #     self.assertFalse()

    def test8(self):
        # get the search textbox
        self.search_field = self.driver.find_element_by_xpath('//*[@id="root"]/div[2]/span')
     
        self.assertEqual(self.search_field.text, "MyNutriPal")

    def test9(self):
        # get the search textbox
        self.search_field = self.driver.find_element_by_xpath('//*[@id="root"]/div[1]/nav/div/a')
    
        self.assertEqual(self.search_field.text, "MyNutripal")


    def test10(self):
        # get the search textbox
        self.search_field = self.driver.find_element_by_xpath('//*[@id="root"]/div[2]/span')
     
        self.assertEqual(self.search_field.text, "MyNutriPal")


    def tearDown(self):
        # close the browser window
        self.driver.quit()

if __name__ == '__main__':
    unittest.main()